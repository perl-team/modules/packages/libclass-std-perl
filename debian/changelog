libclass-std-perl (0.013-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libclass-std-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 09:14:47 +0100

libclass-std-perl (0.013-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 11 Jun 2022 22:48:49 +0100

libclass-std-perl (0.013-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 01 Jan 2021 17:15:13 +0100

libclass-std-perl (0.013-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Nuno Carvalho ]
  * New upstream release
  * Drop POD patch, applied upstream
  * debian/control:
    + update standards version to 3.9.6
    + add explicit libmodule-build-perl dependency
    + Drop build dependencies on libtest-pod*
    + add "Testsuite: autopkgtest-pkg-perl"
  * Install upstream demo as examples
  * Add debian/upstream/metadata information file

 -- Nuno Carvalho <smash@cpan.org>  Fri, 05 Jun 2015 02:37:35 +0100

libclass-std-perl (0.011-1) unstable; urgency=low

  * Team upload.

  [ Jonathan Yu ]
  * New upstream release
  * Use new short rules format
  * Dependency on version.pm was dropped upstream

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/watch: remove obsolete comment.
  * Switch to "3.0 (quilt)" source format.
  * Add patch to fix POD issue. (Closes: #709765)
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Bump debhelper compatibility level to 8.
  * Set Standards-Version to 3.9.4 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 25 May 2013 20:19:31 +0200

libclass-std-perl (0.0.9-2) unstable; urgency=low

  * add libtest-perl and libtest-coverage-perl to b-d-i

 -- Damyan Ivanov <dmn@debian.org>  Tue, 06 May 2008 16:28:58 +0300

libclass-std-perl (0.0.9-1) unstable; urgency=low

  * Take over for the Debian Perl Group; Closes: #479669 -- RFA

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Ivan Kohler <ivan-
    Uploaders.
  * debian/watch: use dist-based URL.
  * add myself to Uploaders

  * New upstream release
  * refreh debian/rules using current dh-make-perl template
    + do not install README as it is useless
    + install examples in demo/
  * Bump Standards-Version to 3.7.3 (no changes)
  * move libversion-perl to B-D-I

 -- Damyan Ivanov <dmn@debian.org>  Tue, 06 May 2008 16:18:25 +0300

libclass-std-perl (0.0.8-3) unstable; urgency=low

  * Really add libversion-perl to Depends and Build-Depends, doh!
    (closes: Bug#423791)

 -- Ivan Kohler <ivan-debian@420.am>  Tue, 12 Jun 2007 10:30:07 -0700

libclass-std-perl (0.0.8-2) unstable; urgency=low

  * Added libversion-perl to Depends and Build-Depends (closes: Bug#423791)

 -- Ivan Kohler <ivan-debian@420.am>  Mon, 14 May 2007 11:27:39 -0700

libclass-std-perl (0.0.8-1) unstable; urgency=low

  * Initial Release (closes: Bug#419966).

 -- Ivan Kohler <ivan-debian@420.am>  Wed, 18 Apr 2007 19:52:26 -0700
